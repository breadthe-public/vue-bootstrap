# vue-bootstrap

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

### Install other dependencies
#### [Bootstrap 4.1.2](http://getbootstrap.com/)
```
yarn add bootstrap
yarn add node-sass sass-loader --dev
```

#### [Font Awesome 4.7.0](https://fontawesome.com/v4.7.0/)
```
yarn add font-awesome
```